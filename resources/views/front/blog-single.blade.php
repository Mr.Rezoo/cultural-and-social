@extends('panel.page')

@section('title')
جزئیات بلاگ
    @endsection

@section('breadcrumb')
    <div class="page-header-left">
        <h3>وبلاگ تکی</h3>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="index.html"><i data-feather="home"></i></a></li>
            <li class="breadcrumb-item">وبلاگ</li>
            <li class="breadcrumb-item active">وبلاگ تکی</li>
            <li class="breadcrumb-item active">{{$article->name}}</li>
        </ol>
    </div>
@endsection

@section('content')
    <!-- Container-fluid starts-->
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="blog-single">
                    <div class="blog-box blog-details"><img class="img-fluid w-100"
                                                            src="{{url('/panel/assets/images/blog/blog-single.jpg')}}" alt="blog-main">
                        <div class="blog-details">
                            <ul class="blog-social">
                                <li class="digits"> تاریخ : {!! jdate($article->created_at)->format('%d %B %Y'); !!}</li>
                                <li><i class="icofont icofont-user"></i><span>{{$article->user->name}}</span></li>
                                <li class="digits"><i class="icofont icofont-thumbs-up"></i><span>بازدید:</span>{{$article->hit}}</li>
                                <li class="digits"><i class="icofont icofont-ui-chat"></i>598 نظر</li>
                            </ul>
                            <h4>
                                {{$article->name}}
                            </h4>
                            <div class="single-blog-content-top">
                                <p>
                                    {!! $article->description !!}
                                </p>
                            </div>
                        </div>
                    </div>
                    <section class="comment-box" data-action="">
                        <h4>نظر</h4>
                        <hr>
                        <ul>
                            @foreach($comments as $comment)
                            <li>
                                <div class="media align-self-center"><img class="align-self-center"
                                                                          src="{{url('/panel/assets/images/blog/comment.jpg')}}"
                                                                          alt="Generic placeholder image">
                                    <div class="media-body">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <h6 class="mt-0">{{$comment->name}}<span> ( طراح )</span></h6>
                                            </div>
                                            <div class="col-md-8">
                                                <ul class="comment-social float-left float-md-right">
                                                    <li class="digits"><i class="icofont icofont-thumbs-up"></i>02
                                                        بازدید
                                                    </li>
                                                    <li class="digits"><i class="icofont icofont-ui-chat"></i>598 نظر
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <p> {{$comment->body}}</p>
                                    </div>
                                </div>
                            </li>
                                @endforeach
                        </ul>
                    </section>
                    <div class="contact-form card-body">
                        <form class="theme-form" action="{{route('comment.store',$article->slug)}}" method="POST">
                          @csrf
                            <div class="form-icon">
                                <i class="icofont icofont-envelope-open"></i>
                            </div>
                            @auth
                            <div class="form-group">
                                <label for="exampleInputName">نام</label>
                                <input type="text" class="form-control" name="name" id="exampleInputName" value="{{Auth::user()->name}}" placeholder="John Dio" readonly>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1" class="col-form-label">ایمیل</label>
                                <input type="email" class="form-control" name="email" id="exampleInputEmail1" value="{{Auth::user()->email}}" placeholder="Demo@gmail.com" readonly>
                            </div>
                            @else
                                <div class="form-group">
                                    <label for="exampleInputName">نام</label>
                                    <input type="text" class="form-control" name="name" id="exampleInputName"  placeholder="John Dio" >
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1" class="col-form-label">ایمیل</label>
                                    <input type="email" class="form-control" name="email" id="exampleInputEmail1" placeholder="Demo@gmail.com" >
                                </div>
                            @endauth
                            <div class="form-group">
                                <label for="body" class="col-form-label">پیام</label>
                                <textarea name="body" row="3" cols="50" class="form-control textarea" placeholder="Your Message"></textarea>
                            </div>
                            <div class="text-sm-right">
                                <button class="btn btn-primary-gradien">ارسال</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid Ends-->
@endsection
