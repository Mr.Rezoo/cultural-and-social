@extends('panel.page')

@section('title')
    صفحه بلاگ ها
@endsection

@section('breadcrumb')
    <div class="page-header-left">
        <h3>جزئیات وبلاگ</h3>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#"><i data-feather="home"></i></a></li>
            <li class="breadcrumb-item">وبلاگ</li>
            <li class="breadcrumb-item active">جزئیات وبلاگ</li>
        </ol>
    </div>
@endsection

@section('content')

    <!-- Container-fluid starts-->
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-6 set-col-12">
                <div class="card">
                    <div class="blog-box blog-shadow"><img class="img-fluid" src="{{url('panel/assets/images/blog/blog.jpg')}}" alt="">
                        <div class="blog-details">
                            <p class="digits">25 خرداد 1398</p>
                            <h4>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ.</h4>
                            <ul class="blog-social">
                                <li><i class="icofont icofont-user"></i>آرش خادملو</li>
                                <li class="digits"><i class="icofont icofont-thumbs-up"></i>02 بازدید</li>
                                <li class="digits"><i class="icofont icofont-ui-chat"></i>598 نظر</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-6 set-col-12">
                <div class="card">
                    <div class="blog-box blog-list row">
                        <div class="col-sm-5"><img class="img-fluid sm-100-w" src="{{url('panel/assets/images/blog/blog-2.jpg')}}" alt=""></div>
                        <div class="col-sm-7">
                            <div class="blog-details">
                                <div class="blog-date digits"><span>02</span> خرداد 1398</div>
                                <h6>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ </h6>
                                <div class="blog-bottom-content">
                                    <ul class="blog-social">
                                        <li>توسط: مدیر</li>
                                        <li class="digits">0 بازدید</li>
                                    </ul>
                                    <hr>
                                    <p class="mt-0">حقیقت، سازنده خوشبختی انسانی کافی است. هیچ یک از لذت برای لذت
                                        بردن.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="blog-box blog-list row">
                        <div class="col-sm-5"><img class="img-fluid sm-100-w"
                                                   src="{{url('panel/assets/images/blog/blog-3.jpg')}}"
                                                   alt=""></div>
                        <div class="col-sm-7">
                            <div class="blog-details">
                                <div class="blog-date digits"><span>03</span> خرداد 1398</div>
                                <h6>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ </h6>
                                <div class="blog-bottom-content">
                                    <ul class="blog-social">
                                        <li>توسط: مدیر</li>
                                        <li class="digits">02 Hits</li>
                                    </ul>
                                    <hr>
                                    <p class="mt-0">حقیقت، سازنده خوشبختی انسانی کافی است. هیچ یک از لذت برای لذت
                                        بردن.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @foreach($articles as $article)
            <div class="col-md-6 col-xl-3 set-col-6">
                <div class="card">
                    <div class="blog-box blog-grid text-center">
                        <img class="img-fluid top-radius-blog" src="@php '/storage/app/public/photos/2/thumbs/4GJUsxX.jpg'.basename($article->image) @endphp" alt="">
                        <div class="blog-details-main">
                            <ul class="blog-social">
                                <li class="digits">{!! jdate($article->created_at)->format('%d %B %Y'); !!}</li>
                                <li class="digits">توسط: {{ $article->user->name }} </li>
                                <li class="digits"> بازدید: {{$article->hit}}</li>
                            </ul>
                            <hr>
                            <h3><a href="{{route('articles.detail',$article->slug)}}">{{$article->name}}</a></h3>
                            <h6 class="blog-bottom-details">@php  echo mb_substr(strip_tags($article->description),0,100,'UTF8') @endphp</h6>
                        </div>
                    </div>
                </div>
            </div>
           @endforeach
        </div>
        {{$articles->links()}}
    </div>
    <!-- Container-fluid Ends-->

@endsection
