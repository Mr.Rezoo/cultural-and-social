{{--     هر محتوایی که بخواییم اینجا قرار بگیر از دستوره yield استفاده میکنیم( و در ویو خودش از section)--}}

    <!DOCTYPE html>
<html lang="en" dir="rtl">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description"
          content="endless admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
    <meta name="keywords"
          content="admin template, endless admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="pixelstrap">
    <link rel="icon" href='{{url("/panel/assets/images/favicon.png")}}' type="image/x-icon">
    <link rel="shortcut icon" href='{{url("/panel/assets/images/favicon.png")}}' type="image/x-icon">
    <title>Endless - Premium Admin Template</title>
    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Work+Sans:100,200,300,400,500,600,700,800,900" rel="stylesheet">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">
    <link
        href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">
    <!-- Font Awesome-->
    <link rel="stylesheet" type="text/css" href='{{url("/panel/assets/css/fontawesome.css")}}'>
    <!-- ico-font-->
    <link rel="stylesheet" type="text/css" href='{{url("/panel/assets/css/icofont.css")}}'>
    <!-- Themify icon-->
    <link rel="stylesheet" type="text/css" href='{{url("/panel/assets/css/themify.css")}}'>
    <!-- Flag icon-->
    <link rel="stylesheet" type="text/css" href='{{url("/panel/assets/css/flag-icon.css")}}'>
    <!-- Feather icon-->
    <link rel="stylesheet" type="text/css" href='{{url("/panel/assets/css/feather-icon.css")}}'>
    <!-- Plugins css start-->
    <link rel="stylesheet" type="text/css" href='{{url("/panel/assets/css/jsgrid.css")}}'>
    <!-- Plugins css Ends-->
    <!-- Bootstrap css-->
    <link rel="stylesheet" type="text/css" href='{{url("/panel/assets/css/bootstrap.css")}}'>
    <!-- App css-->
    <link rel="stylesheet" type="text/css" href='{{url("/panel/assets/css/style.css")}}'>
    <link id="color" rel="stylesheet" href='{{url("/panel/assets/css/light-1.css")}}' media="screen">
    <!-- Responsive css-->
    <link rel="stylesheet" type="text/css" href='{{url("/panel/assets/css/responsive.css")}}'>
</head>
<body main-theme-layout="rtl">
<!-- Loader starts-->
<div class="loader-wrapper">
    <div class="loader bg-white">
        <div class="whirly-loader"></div>
    </div>
</div>
<!-- Loader ends-->
<!-- page-wrapper Start-->
<div class="page-wrapper">
    <!-- Page Header Start-->

    <!-- Page Header Ends                              -->
    <!-- Page Body Start-->
    <div class="page-body-wrapper">
        <!-- Page Sidebar Start-->

        <!-- Page Sidebar Ends-->
        <!-- Right sidebar Start-->

        <!-- Right sidebar Ends-->
        <div class="page-body">
            <div class="container-fluid">
                <div class="page-header">
                    <div class="row">
                        <div class="col">
                            <div class="page-header-left">
                                <h3>جداول شبکه ای جی اس</h3>
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="index.html"><i data-feather="home"></i></a>
                                    </li>
                                    <li class="breadcrumb-item">جداول</li>
                                    <li class="breadcrumb-item active">جداول شبکه ای جی اس</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Container-fluid starts-->
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-header">
                                <h5>سناریوی اصلی</h5><span>شبکه با فیلتر کردن، ویرایش، اضافه کردن، حذف، مرتب سازی و صفحه بندی. داده های ارائه شده توسط کنترل کننده.</span>
                            </div>
                            <div class="card-body">
                                <div id="basicScenario"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Container-fluid Ends-->
        </div>
        <!-- footer start-->

    </div>
</div>
<!-- latest jquery-->
<script src='{{url("/panel/assets/js/jquery-3.2.1.min.js")}}'></script>
<!-- Bootstrap js-->
<script src='{{url("/panel/assets/js/bootstrap/popper.min.js")}}'></script>
<script src='{{url("/panel/assets/js/bootstrap/bootstrap.js")}}'></script>
<!-- feather icon js-->
<script src='{{url("/panel/assets/js/icons/feather-icon/feather.min.js")}}'></script>
<script src='{{url("/panel/assets/js/icons/feather-icon/feather-icon.js")}}'></script>
<!-- Sidebar jquery-->
<script src='{{url("/panel/assets/js/sidebar-menu.js")}}'></script>
<script src='{{url("/panel/assets/js/config.js")}}'></script>
<!-- Plugins JS start-->
<script src='{{url("/panel/assets/js/jsgrid/jsgrid.min.js")}}'></script>
<script src='{{url("/panel/assets/js/jsgrid/griddata.js")}}'></script>
<script src='{{url("/panel/assets/js/jsgrid/jsgrid.js")}}'></script>
<script src='{{url("/panel/assets/js/typeahead/handlebars.js")}}'></script>
<script src='{{url("/panel/assets/js/typeahead/typeahead.bundle.js")}}'></script>
<script src='{{url("/panel/assets/js/typeahead/typeahead.custom.js")}}'></script>
<script src='{{url("/panel/assets/js/chat-menu.js")}}'></script>
<script src='{{url("/panel/assets/js/tooltip-init.js")}}'></script>
<script src='{{url("/panel/assets/js/typeahead-search/handlebars.js")}}'></script>
<script src='{{url("/panel/assets/js/typeahead-search/typeahead-custom.js")}}'></script>
<!-- Plugins JS Ends-->
<!-- Theme js-->
<script src='{{url("/panel/assets/js/script.js")}}'></script>
<script src='{{url("/panel/assets/js/theme-customizer/customizer.js")}}'></script>
<!-- Plugin used-->
</body>

</html>
