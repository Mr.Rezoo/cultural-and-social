@extends('panel.page')

@section('title')
    پنل مدیریت - مدیریت مطالب
@endsection


@section('content')
    {{--     هر محتوایی که بخواییم اینجا قرار بگیر از دستوره yield استفاده میکنیم( و در ویو خودش از section)--}}
    <a href="{{route('admin.articles.create')}}" class="btn btn-success">مطلب جدید</a>
    <div class="row">
        <div class="col-lg-12 grid-align align-content-stretch ">
            <div class="card">
                <div class="card-body">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>نام</th>
                            <th>نام مستعار - Slug</th>
                            <th>نویسنده</th>
                            <th>دسته بندی</th>
                            <th>بازدید</th>
                            <th>وضعیت</th>
                            <th>مدیریت</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($articles as $article)

                            @switch($article->status)
                                @case(1)
                                @php
                                    $url = route('admin.articles.status',$article->id);
                                    $status = '<a href= "'.$url.'" class="badge badge-success">فعال</a>'@endphp
                                @break
                                @case(0)
                                @php
                                    $url = route('admin.articles.status',$article->id);
                                    $status = '<a href= "'.$url.'" class="badge badge-danger">غیر فعال</a>'@endphp
                                @break
                                @default
                                @endswitch

                            <tr>
                                <td>{{$article->name}}</td>
                                <td>{{$article->slug}}</td>
                                <td>{{$article->user->name}}</td>
                                <td>
                                    @foreach($article->categories()->pluck('name') as $category)
                                        <span class="badge badge-dark">{{$category}}</span>
                                        @endforeach
                                </td>
                                <td>{{$article->hit}}</td>
                                <td>{!!$status!!}</td>

                                <td>
                                    <a href="{{route('admin.articles.edit',$article->id)}}"
                                       class="badge badge-info">ویرایش</a>
                                    <a href="{{route('admin.articles.destroy',$article->id)}}"
                                       class="badge badge-warning"
                                       onclick="return confirm('آیا آیتم مورد نظر حذف شود؟؟!!')">حذف</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
        {{$articles->links()}}
    </div>


@endsection
