<!DOCTYPE html>
<html lang="en" dir="rtl">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="endless admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
    <meta name="keywords" content="admin template, endless admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="pixelstrap">
    <link rel="icon" href='{{url("/panel/assets/images/favicon.png")}}' type="image/x-icon">
    <link rel="shortcut icon" href='{{url("/panel/assets/images/favicon.png")}}' type="image/x-icon">
    <title>@yield('title')</title>
    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Work+Sans:100,200,300,400,500,600,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <!-- Font Awesome-->
    <link rel="stylesheet" type="text/css" href='{{url("/panel/assets/css/fontawesome.css")}}'>
    <!-- ico-font-->
    <link rel="stylesheet" type="text/css" href='{{url("/panel/assets/css/icofont.css")}}'>
    <!-- Themify icon-->
    <link rel="stylesheet" type="text/css" href='{{url("/panel/assets/css/themify.css")}}'>
    <!-- Flag icon-->
    <link rel="stylesheet" type="text/css" href='{{url("/panel/assets/css/flag-icon.css")}}'>
    <!-- Feather icon-->
    <link rel="stylesheet" type="text/css" href='{{url("/panel/assets/css/feather-icon.css")}}'>
    <!-- Plugins css start-->
    <link rel="stylesheet" type="text/css" href='{{url("/panel/assets/css/jsgrid.css")}}'>
    <link rel="stylesheet" type="text/css" href='{{url("/panel/assets/css/select2.css")}}'>
    <link rel="stylesheet" type="text/css" href='{{url("/panel/assets/css/dropzone.css")}}'>
    <!-- Plugins css Ends-->
    <!-- Bootstrap css-->
    <link rel="stylesheet" type="text/css" href='{{url("/panel/assets/css/bootstrap.css")}}'>
    <!-- App css-->
    <link rel="stylesheet" type="text/css" href='{{url("/panel/assets/css/style.css")}}'>
    <link id="color" rel="stylesheet" href='{{url("/panel/assets/css/light-1.css")}}' media="screen">
    <!-- Responsive css-->
    <link rel="stylesheet" type="text/css" href='{{url("/panel/assets/css/responsive.css")}}'>
    <script src="{{url('//cdn.tinymce.com/4/tinymce.min.js')}}"></script>
    <script>

        var editor_config = {
            path_absolute : "/",
            selector: "textarea#editor",
            directionality :"rtl",
            plugins: [
                "directionality advlist autolink lists link image charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars code fullscreen",
                "insertdatetime media nonbreaking save table contextmenu directionality",
                "emoticons template paste textcolor colorpicker textpattern"
            ],
            toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media | ltr rtl",
            relative_urls: false,
            file_browser_callback : function(field_name, url, type, win) {
                var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
                var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

                var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
                if (type == 'image') {
                    cmsURL = cmsURL + "&type=Images";
                } else {
                    cmsURL = cmsURL + "&type=Files";
                }

                tinyMCE.activeEditor.windowManager.open({
                    file : cmsURL,
                    title : 'Filemanager',
                    width : x * 0.8,
                    height : y * 0.8,
                    resizable : "yes",
                    close_previous : "no"
                });
            }
        };

        tinymce.init(editor_config);
    </script>
  </head>
  <body main-theme-layout="rtl">
    <!-- Loader starts-->
    <div class="loader-wrapper">
      <div class="loader bg-white">
        <div class="whirly-loader"> </div>
      </div>
    </div>
    <!-- Loader ends-->
    <!-- page-wrapper Start-->
    <div class="page-wrapper">
      <!-- Page Header Start-->
     @include('panel.navbar')
      <!-- Page Header Ends                              -->
      <!-- Page Body Start-->
      <div class="page-body-wrapper">
        <!-- Page Sidebar Start-->
      @include('panel.slidebar')
        <!-- Page Sidebar Ends-->
        <!-- Right sidebar Start-->
        @include('panel.rSlidebar')
        <!-- Right sidebar Ends-->
        <div class="page-body">
          <div class="container-fluid">
            <div class="page-header">
              <div class="row">
                <div class="col">
                  @yield('breadcrumb')
                </div>

                <!-- Bookmark Start-->
                  <div class="col">
                      <div class="bookmark pull-right">

                          <ul>
                              <li><a href="#" data-container="body" data-toggle="popover" data-placement="top" title="" data-original-title="تقویم"><i data-feather="calendar"></i></a></li>
                              <li><a href="#" data-container="body" data-toggle="popover" data-placement="top" title="" data-original-title="ایمیل"><i data-feather="mail"></i></a></li>
                              <li><a href="#" data-container="body" data-toggle="popover" data-placement="top" title="" data-original-title="چت"><i data-feather="message-square"></i></a></li>
                              <li><a href="#"><i class="bookmark-search" data-feather="star"></i></a>
                                  <form class="form-inline search-form">
                                      <div class="form-group form-control-search">
                                          <input type="text" placeholder="جستجو..">
                                      </div>
                                  </form>
                              </li>
                          </ul>
                      </div>
                  </div>


                  <!-- Bookmark Ends-->
              </div>
            </div>
          </div>
          <!-- Container-fluid starts-->
            @include('panel.messages')
       @yield('content')

        </div>
        <!-- footer start-->
          @include('panel.footer')
      </div>

    </div>
    <!-- latest jquery-->
    <script src='{{url("/panel/assets/js/jquery-3.2.1.min.js")}}'></script>
    <!-- Bootstrap js-->
    <script src='{{url("/panel/assets/js/bootstrap/popper.min.js")}}'></script>
    <script src='{{url("/panel/assets/js/bootstrap/bootstrap.js")}}'></script>
    <!-- feather icon js-->
    <script src='{{url("/panel/assets/js/icons/feather-icon/feather.min.js")}}'></script>
    <script src='{{url("/panel/assets/js/icons/feather-icon/feather-icon.js")}}'></script>
    <!-- Sidebar jquery-->
    <script src='{{url("/panel/assets/js/sidebar-menu.js")}}'></script>
    <script src='{{url("/panel/assets/js/config.js")}}'></script>
    <!-- Plugins JS start-->
    <script src='{{url("/panel/assets/js/select2/select2.full.min.js")}}'></script>
    <script src='{{url("/panel/assets/js/select2/select2-custom.js")}}'></script>
    <script src='{{url("/panel/assets/js/editor/ckeditor/ckeditor.js")}}'></script>
    <script src='{{url("/panel/assets/js/editor/ckeditor/styles.js")}}'></script>
    <script src='{{url("/panel/assets/js/editor/ckeditor/adapters/jquery.js")}}'></script>
    <script src='{{url("/panel/assets/js/dropzone/dropzone.js")}}'></script>
    <script src='{{url("/panel/assets/js/dropzone/dropzone-script.js")}}'></script>
    <script src='{{url("/panel/assets/js/jsgrid/jsgrid.min.js")}}'></script>
    <script src='{{url("/panel/assets/js/jsgrid/griddata.js")}}'></script>
    <script src='{{url("/panel/assets/js/jsgrid/jsgrid.js")}}'></script>
    <script src='{{url("/panel/assets/js/typeahead/handlebars.js")}}'></script>
    <script src='{{url("/panel/assets/js/typeahead/typeahead.bundle.js")}}'></script>
    <script src='{{url("/panel/assets/js/typeahead/typeahead.custom.js")}}'></script>
    <script src='{{url("/panel/assets/js/chat-menu.js")}}'></script>
    <script src='{{url("/panel/assets/js/tooltip-init.js")}}'></script>
    <script src='{{url("/panel/assets/js/typeahead-search/handlebars.js")}}'></script>
    <script src='{{url("/panel/assets/js/typeahead-search/typeahead-custom.js")}}'></script>
    <!-- Plugins JS Ends-->
    <!-- Theme js-->
    <script src='{{url("/panel/assets/js/script.js")}}'></script>
    <script src='{{url("/panel/assets/js/theme-customizer/customizer.js")}}'></script>
    <!-- Plugin used-->
  </body>

</html>
