@extends('panel.page')

@section('title')
    پنل مدیریت - مدیریت مطالب
@endsection


@section('content')
    {{--     هر محتوایی که بخواییم اینجا قرار بگیر از دستوره yield استفاده میکنیم( و در ویو خودش از section)--}}
    <div class="row">
        <div class="col-lg-12 grid-align align-content-stretch ">
            <div class="card">
                <div class="card-body">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>خلاصه نظر </th>
                            <th>نویسنده</th>
                            <th>مطلب</th>
                            <th>وضعیت</th>
                            <th>مدیریت</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($comments as $comment)

                            @switch($comment->status)
                                @case(1)
                                @php
                                    $url = route('admin.comments.status',$comment->id);
                                    $status = '<a href= "'.$url.'" class="badge badge-success">فعال</a>'@endphp
                                @break
                                @case(0)
                                @php
                                    $url = route('admin.comments.status',$comment->id);
                                    $status = '<a href= "'.$url.'" class="badge badge-danger">غیر فعال</a>'@endphp
                                @break
                                @default
                                @endswitch

                            <tr>
                                <td>{{$comment->body}}</td>
                                <td>{{$comment->name}}</td>
                                <td>{{$comment->article_id}}</td>
                                <td>{!!$status!!}</td>

                                <td>
                                    <a href="{{route('admin.comments.edit',$comment->id)}}"
                                       class="badge badge-info">ویرایش</a>
                                    <a href="{{route('admin.comments.destroy',$comment->id)}}"
                                       class="badge badge-warning"
                                       onclick="return confirm('آیا آیتم مورد نظر حذف شود؟؟!!')">حذف</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
        {{$comments->links()}}
    </div>


@endsection
