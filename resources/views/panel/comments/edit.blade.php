@extends('panel.page')

@section('title')
   مدیریت نظرات
@endsection

@section('content')
    <!-- Container-fluid starts-->
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>ویرایش مطلب</h5>
                    </div>
                    <div class="card-body add-post">
                        <form id="main-frm" class="row needs-validation" novalidate=""
                              action="{{route('admin.articles.update',$article->id)}}" method="post">
                            @csrf
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="validationCustom01">عنوان:</label>
                                    <input class="form-control @error('name') is-invalid @enderror" name="name"
                                           id="validationCustom01" type="text" placeholder="عنوان پست" required=""
                                           value="{{$article->name}}">
                                    <div class="valid-feedback">به نظر خوب میاد!</div>
                                    @error('name')
                                    <div class="alert alert-">{{$message}}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="validationCustom01">نام مستعار- Slug</label>
                                    <input class="form-control @error('slug') is-invalid @enderror" name="slug"
                                           id="validationCustom01" type="text" placeholder="عنوان پست" required=""
                                           value="{{$article->slug}}">
                                    @error('slug')
                                    <div class="alert alert-">{{$message}}</div>
                                    @enderror
                                </div>
                                {{--                        <div class="form-group">--}}
                                {{--                          <label>نوع:</label>--}}
                                {{--                          <div class="m-checkbox-inline">--}}
                                {{--                            <label for="edo-ani">--}}
                                {{--                              <input class="radio_animated" id="edo-ani" type="radio" name="rdo-ani" checked="">متن--}}
                                {{--                            </label>--}}
                                {{--                            <label for="edo-ani1">--}}
                                {{--                              <input class="radio_animated" id="edo-ani1" type="radio" name="rdo-ani">تصویر--}}
                                {{--                            </label>--}}
                                {{--                            <label for="edo-ani2">--}}
                                {{--                              <input class="radio_animated" id="edo-ani2" type="radio" name="rdo-ani" checked="">صوتی--}}
                                {{--                            </label>--}}
                                {{--                            <label for="edo-ani3">--}}
                                {{--                              <input class="radio_animated" id="edo-ani3" type="radio" name="rdo-ani">ویدئو--}}
                                {{--                            </label>--}}
                                {{--                          </div>--}}
                                {{--                        </div>--}}
                                <div class="form-group">
                                    <div class="col-form-label">دسته بندی:
                                        <select class="js-example-placeholder-multiple col-sm-12" multiple="multiple"
                                                name="categories[]">
                                            @foreach($categories as $cat_id => $cat_name)
                                                <option value="{{$cat_id}}"
                                                    @php if(in_array($cat_id,$article->categories->pluck('id')->toArray())) echo 'selected'; @endphp
                                                >{{$cat_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="email-wrapper">
                                    <div class="theme-form">
                                        <div class="form-group">
                                            <label>محتوا:</label>
                                            <textarea id="editor"
                                                      class="form-control @error('description') is-invalid @enderror "
                                                      name="description" cols="10"
                                                      rows="3">{{$article->description}}</textarea>
                                            @error('description')
                                            <div class="alert alert-">{{$message}}</div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="validationCustom01">وضعیت</label>
                                    <select class="form-control @error('status') is-invalid @enderror" name="status">
                                        <option value="0">منتشر نشده</option>
                                        <option value="1" @php if($article->status==1) echo'selected'; @endphp>منتشر
                                            شده
                                        </option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="validationCustom01"> نویسنده : {{Auth::user()->name}} </label>
                                    <input name="user_id" type="hidden" value="{{Auth::user()->id}}">
                                </div>
                            </div>

                        </form>
                        <form method="post" class="dropzone digits" id="singleFileUpload" action="{{ route('api-upload') }}">
                            <div class="m-0 dz-message needsclick"><i class="icon-cloud-up"></i>
                                <h6 class="mb-0">فایل ها را رها کنید یا برای آپلود کلیک کنید.</h6>
                            </div>
                        </form>
                        <div class="btn-showcase">
                            <button class="btn btn-primary" type="submit" onclick="$('#main-frm').submit()">پست</button>
                            <button href="{{route('admin.articles')}}" class="btn btn-light" type="submit">نادیده
                                گرفتن
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid Ends-->

    <script>
        document.getElementById("singleFileUpload").addEventListener('submit', functSubmit);

        function functSubmit(event) {
            var msg = document.getElementById("input1").value;
            alert(msg);
        }
    </script>
@endsection
