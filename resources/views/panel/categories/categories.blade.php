@extends('panel.page')

@section('title')
    پنل مدیریت - مدیریت دسته بندی ها
@endsection


@section('content')
    {{--     هر محتوایی که بخواییم اینجا قرار بگیر از دستوره yield استفاده میکنیم( و در ویو خودش از section)--}}
    <a href="{{route('admin.categories.create')}}" class="btn btn-success">دسته بندی جدید</a>
    <div class="row">
        <div class="col-lg-12 grid-align align-content-stretch ">
            <div class="card">
                <div class="card-body">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>نام</th>
                            <th>نام مستعار - Slug</th>
                            <th>مدیریت</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($categories as $category)
                            <tr>
                                <td>{{$category->name}}</td>
                                <td>{{$category->slug}}</td>
                                <td>
                                    <a href="{{route('admin.categories.edit',$category->id)}}"
                                       class="badge badge-info">ویرایش</a>
                                    <a href="{{route('admin.categories.destroy',$category->id)}}"
                                       class="badge badge-warning"
                                       onclick="return confirm('آیا آیتم مورد نظر حذف شود؟؟!!')">حذف</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>

                    </table>
                </div>
            </div>

        </div>
        {{$categories->links()}}
    </div>

@endsection
