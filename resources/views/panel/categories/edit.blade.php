@extends('panel.page')

@section('title')
    ویرایش دسته بندی
    @endsection

@section('content')
          <!-- Container-fluid starts-->

          <div class="container-fluid">
            <div class="edit-profile">
              <div class="row">
                <div class="col-lg-4">
                  <div class="card">
                    <div class="card-header">
                      <h4 class="card-title mb-0">دسته بندی</h4>
                      <div class="card-options"><a class="card-options-collapse" href="#" data-toggle="card-collapse"><i class="fe fe-chevron-up"></i></a><a class="card-options-remove" href="#" data-toggle="card-remove"><i class="fe fe-x"></i></a></div>
                    </div>
                    <div class="card-body">
                      <form action="{{route('admin.categories.update',$category->id)}}" method="post">
                          @csrf
                        <div class="row mb-2">
                          <div class="col-auto"><img class="img-70 rounded-circle" alt="" src='{{url("/panel/assets/images/user/7.jpg")}}'></div>
                          <div class="col">
                            <h3 class="mb-1"></h3>
                            <p class="mb-4">طراح</p>
                          </div>
                        </div>
                          <div class="form-group">
                              <label class="form-label">نام دسته بندی</label>
                              <input class="form-control @error('name') is-invalid @enderror " name="name" type="text" value="{{$category->name}}">
                              @error('name')
                              <div class="alert alert-danger">{{$message}}</div>
                              @enderror
                          </div>
                        <div class="form-group">
                          <label class="form-label">نام مستعار - Slug</label>
                          <input class="form-control @error('slug') is-invalid @enderror " name="slug"  value="{{$category->slug}}">
                            @error('slug')
                            <div class="alert alert-danger">{{$message}}</div>
                            @enderror
                        </div>

                        <div class="form-footer">
                          <button   type="submit" class="btn btn-primary-gradien btn-block">ذخیره</button>
                            <a href="{{route('admin.categories')}}" class="btn btn-danger-gradien btn-block" >انصراف</a>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- Container-fluid Ends-->
      @endsection
