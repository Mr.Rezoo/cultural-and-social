<div class="page-sidebar">
    <div class="main-header-left d-none d-lg-block">
        <div class="logo-wrapper"><a href="index.html"><img src='{{url("/panel/assets/images/endless-logo.png")}}' alt=""></a></div>
    </div>
    <div class="sidebar custom-scrollbar">
        <div class="sidebar-user text-center">
            <div><img class="img-60 rounded-circle" src='{{url("/panel/assets/images/user/1.jpg")}}' alt="#">
                @auth()
                <div class="profile-edit"><a href="{{route('profile',Auth::user()->id)}}" target="_blank"><i data-feather="edit"></i></a></div>
                @endauth
            </div>
            <h6 class="mt-3 f-14">پارادایم</h6>
            <p>مدیر کل.</p>
        </div>
        <ul class="sidebar-menu">
            <li><a class="sidebar-header" href="#"><i data-feather="home"></i><span>مدیریت</span><span class="badge badge-pill badge-primary">6</span><i class="fa fa-angle-right pull-right"></i></a>
                <ul class="sidebar-submenu">
                    <li><a href="{{route('panel.users')}}"><i class="fa fa-circle"></i><span>کاربران</span></a></li>
                    <li><a href="{{route('admin.categories')}}"><i class="fa fa-circle"></i><span>دسته بندی ها</span></a></li>
                    <li><a href="{{route('admin.articles')}}"><i class="fa fa-circle"></i><span>مطالب</span></a></li>
                    <li><a href="{{route('admin.comments')}}"><i class="fa fa-circle"></i><span>نظرات</span></a></li>
                </ul>
            </li>
            <li><a class="sidebar-header" href="#"><i data-feather="edit"></i><span> وبلاگ</span><i class="fa fa-angle-right pull-right"></i></a>
                <ul class="sidebar-submenu">
                    <li><a href="{{route('articles')}}"><i class="fa fa-circle"></i>جزئیات وبلاگ</a></li>
                    <li><a href="#"><i class="fa fa-circle"></i>وبلاگ تکی</a></li>
                </ul>
            </li>
        </ul>
    </div>
</div>
