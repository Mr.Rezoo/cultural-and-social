<!DOCTYPE html>
<html lang="en" dir="rtl">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="endless admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
    <meta name="keywords" content="admin template, endless admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="pixelstrap">
    <link rel="icon" href='{{url("/panel/assets/images/favicon.png")}}' type="image/x-icon">
    <link rel="shortcut icon" href='{{url("/panel/assets/images/favicon.png")}}' type="image/x-icon">
    <title>Endless - Premium Admin Template</title>
    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Work+Sans:100,200,300,400,500,600,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <!-- Font Awesome-->
    <link rel="stylesheet" type="text/css" href='{{url("/panel/assets/css/fontawesome.css")}}'>
    <!-- ico-font-->
    <link rel="stylesheet" type="text/css" href='{{url("/panel/assets/css/icofont.css")}}'>
    <!-- Themify icon-->
    <link rel="stylesheet" type="text/css" href='{{url("/panel/assets/css/themify.css")}}'>
    <!-- Flag icon-->
    <link rel="stylesheet" type="text/css" href='{{url("/panel/assets/css/flag-icon.css")}}'>
    <!-- Feather icon-->
    <link rel="stylesheet" type="text/css" href='{{url("/panel/assets/css/feather-icon.css")}}'>
    <!-- Plugins css start-->
    <!-- Plugins css Ends-->
    <!-- Bootstrap css-->
    <link rel="stylesheet" type="text/css" href='{{url("/panel/assets/css/bootstrap.css")}}'>
    <!-- App css-->
    <link rel="stylesheet" type="text/css" href='{{url("/panel/assets/css/style.css")}}'>
    <link id="color" rel="stylesheet" href='{{url("/panel/assets/css/light-1.css")}}' media="screen">
    <!-- Responsive css-->
    <link rel="stylesheet" type="text/css" href='{{url("/panel/assets/css/responsive.css")}}'>
  </head>
  <body main-theme-layout="rtl">
    <!-- Loader starts-->
    <div class="loader-wrapper">
      <div class="loader bg-white">
        <div class="whirly-loader"> </div>
      </div>
    </div>
    <!-- Loader ends-->
    <!-- page-wrapper Start-->
    <div class="page-wrapper">
      <!-- Page Header Start-->
    @include('panel.navbar')
      <!-- Page Header Ends                              -->
      <!-- Page Body Start-->
      <div class="page-body-wrapper">
        <!-- Page Sidebar Start-->
        @include('panel.slidebar')
        <!-- Page Sidebar Ends-->
        <!-- Right sidebar Start-->
        @include('panel.rSlidebar')
        <!-- Right sidebar Ends-->
        <div class="page-body">
          <div class="container-fluid">
            <div class="page-header">
              <div class="row">
                <div class="col">
                  <div class="page-header-left">
                    <h3>ویرایش پروفایل</h3>
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item"><a href="index.html"><i data-feather="home"></i></a></li>
                      <li class="breadcrumb-item">کاربران</li>
                      <li class="breadcrumb-item active">ویرایش پروفایل</li>
                    </ol>
                  </div>
                </div>
                <!-- Bookmark Start-->
                  @include('panel.bookmarkStart')
                <!-- Bookmark Ends-->
              </div>
            </div>
          </div>
            @include('panel.messages')
          <!-- Container-fluid starts-->
          <div class="container-fluid">
            <div class="edit-profile">
              <div class="row">
                <div class="col-lg-4">
                  <div class="card">
                    <div class="card-header">
                      <h4 class="card-title mb-0">پروفایل من</h4>
                        @include('panel.messages')
                      <div class="card-options"><a class="card-options-collapse" href="#" data-toggle="card-collapse"><i class="fe fe-chevron-up"></i></a><a class="card-options-remove" href="#" data-toggle="card-remove"><i class="fe fe-x"></i></a></div>
                    </div>
                    <div class="card-body">
                      <form action="{{route('admin.profileUpdate',$user->id)}}" method="post">
                          @csrf
                        <div class="row mb-2">
                          <div class="col-auto"><img class="img-70 rounded-circle" alt="" src='{{url("/panel/assets/images/user/7.jpg")}}'></div>
                          <div class="col">
                            <h3 class="mb-1">{{$user->name}}</h3>
                            <p class="mb-4">طراح</p>
                          </div>
                        </div>
                          <div class="form-group">
                              <label class="form-label">نام و نام خانوادگی</label>
                              <input class="form-control @error('name') is-invalid @enderror " name="name" type="text" value="{{$user->name}}">
                              @error('name')
                              <div class="alert alert-danger">{{$message}}</div>
                              @enderror
                          </div>
                        <div class="form-group">
                          <label class="form-label">آدرس ایمیل</label>
                          <input class="form-control @error('email') is-invalid @enderror " name="email" placeholder="your-email@domain.com" value="{{$user->email}}">
                            @error('email')
                            <div class="alert alert-danger">{{$message}}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                          <label class="form-label">رمزعبور</label>
                          <input class="form-control @error('password') is-invalid @enderror" name="password" type="password" value="password">
                            @error('password')
                            <div class="alert alert-danger">{{$message}}</div>
                            @enderror
                        </div>
                          <div class="form-group">
                              <label class="form-label">تکرار رمزعبور</label>
                              <input class="form-control @error('password_confirmation') is-invalid @enderror" name="password_confirmation" type="password" value="password">
                              @error('password_confirmation')
                              <div class="alert alert-danger">{{$message}}</div>
                              @enderror
                          </div>
                        <div class="form-group">
                          <label class="form-label">شماره موبایل</label>
                          <input class="form-control @error('phone') is-invalid @enderror " name="phone" placeholder="09120000" value="{{$user->phone}}">
                            @error('phone')
                            <div class="alert alert-danger">{{$message}}</div>
                            @enderror
                        </div>
                        <div class="form-footer">
                          <button   type="submit" class="btn btn-primary-gradien btn-block">ذخیره</button>
                            <a href="{{route('panel.users')}}" class="btn btn-danger-gradien btn-block" >انصراف</a>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- Container-fluid Ends-->
        </div>
        <!-- footer start-->
        @include('panel.footer')
      </div>
    </div>
    <!-- latest jquery-->
    <script src='{{url("/panel/assets/js/jquery-3.2.1.min.js")}}'></script>
    <!-- Bootstrap js-->
    <script src='{{url("/panel/assets/js/bootstrap/popper.min.js")}}'></script>
    <script src='{{url("/panel/assets/js/bootstrap/bootstrap.js")}}'></script>
    <!-- feather icon js-->
    <script src='{{url("/panel/assets/js/icons/feather-icon/feather.min.js")}}'></script>
    <script src='{{url("/panel/assets/js/icons/feather-icon/feather-icon.js")}}'></script>
    <!-- Sidebar jquery-->
    <script src='{{url("/panel/assets/js/sidebar-menu.js")}}'></script>
    <script src='{{url("/panel/assets/js/config.js")}}'></script>
    <!-- Plugins JS start-->
    <script src='{{url("/panel/assets/js/typeahead/handlebars.js")}}'></script>
    <script src='{{url("/panel/assets/js/typeahead/typeahead.bundle.js")}}'></script>
    <script src='{{url("/panel/assets/js/typeahead/typeahead.custom.js")}}'></script>
    <script src='{{url("/panel/assets/js/chat-menu.js")}}'></script>
    <script src='{{url("/panel/assets/js/tooltip-init.js")}}'></script>
    <script src='{{url("/panel/assets/js/typeahead-search/handlebars.js")}}'></script>
    <script src='{{url("/panel/assets/js/typeahead-search/typeahead-custom.js")}}'></script>
    <!-- Plugins JS Ends-->
    <!-- Theme js-->
    <script src='{{url("/panel/assets/js/script.js")}}'></script>
    <script src='{{url("/panel/assets/js/theme-customizer/customizer.js")}}'></script>
    <!-- Plugin used-->
  </body>

</html>
