@extends('panel.page')

@section('title')
    پنل مدیریت - مدیریت کاربران
@endsection


@section('content')
    {{--     هر محتوایی که بخواییم اینجا قرار بگیر از دستوره yield استفاده میکنیم( و در ویو خودش از section)--}}
    <div class="row">
        <div class="col-lg-12 grid-align align-content-stretch ">
            <div class="card">
                <div class="card-body">
                    <table class="table table-hover">

                        <thead>
                        <tr>
                            <th>نام</th>
                            <th>ایمیل</th>
                            <th>تلفن</th>
                            <th>نقش</th>
                            <th>وضعیت</th>
                            <th>مدیریت</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($users as $user)
                            @switch($user->id )
                                @case(1)
                                @php $role = 'مدیر' @endphp
                                @break
                                @case(2)
                                @php $role = 'کاربر' @endphp
                                @break
                                @default
                            @endswitch

                            @switch($user->status)
                                @case(1)
                                @php
                                    $url = route('admin.user.status',$user->id);
                                    $status = '<a href= "'.$url.'" class="badge badge-success">فعال</a>'@endphp
                                @break
                                @case(0)
                                @php
                                    $url = route('admin.user.status',$user->id);
                                    $status = '<a href= "'.$url.'" class="badge badge-danger">غیر فعال</a>'@endphp
                                @break
                                @default
                            @endswitch

                            <tr>
                                <td>{{$user->name}}</td>
                                <td>{{$user->email}}</td>
                                <td>{{$user->phone}}</td>
                                <td>{{$role}}</td>
                                <td>{!!$status!!}</td>
                                <td>
                                    <a href="{{route('admin.profile',$user->id)}}"
                                       class="badge badge-info">ویرایش</a>
                                    <a href="{{route('admin.user.delete',$user->id)}}" class="badge badge-warning"
                                       onclick="return confirm('آیا کاربر مورد نظر حذف شود؟؟!!')">حذف</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            {{$users->links()}}
        </div>
    </div>

@endsection
