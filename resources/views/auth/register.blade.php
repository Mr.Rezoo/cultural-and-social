<!DOCTYPE html>
<html lang="en" dir="rtl">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="endless admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
    <meta name="keywords" content="admin template, endless admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="pixelstrap">
    <link rel="icon" href='{{url("/panel/assets/images/favicon.png")}}' type="image/x-icon">
    <link rel="shortcut icon" href='{{url("/panel/assets/images/favicon.png")}}' type="image/x-icon">
    <title>فرم ثبت نام</title>
    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Work+Sans:100,200,300,400,500,600,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <!-- Font Awesome-->
    <link rel="stylesheet" type="text/css" href='{{url("/panel/assets/css/fontawesome.css")}}'>
    <!-- ico-font-->
    <link rel="stylesheet" type="text/css" href='{{url("/panel/assets/css/icofont.css")}}'>
    <!-- Themify icon-->
    <link rel="stylesheet" type="text/css" href='{{url("/panel/assets/css/themify.css")}}'>
    <!-- Flag icon-->
    <link rel="stylesheet" type="text/css" href='{{url("/panel/assets/css/flag-icon.css")}}'>
    <!-- Feather icon-->
    <link rel="stylesheet" type="text/css" href='{{url("/panel/assets/css/feather-icon.css")}}'>
    <!-- Plugins css start-->
    <!-- Plugins css Ends-->
    <!-- Bootstrap css-->
    <link rel="stylesheet" type="text/css" href='{{url("/panel/assets/css/bootstrap.css")}}'>
    <!-- App css-->
    <link rel="stylesheet" type="text/css" href='{{url("/panel/assets/css/style.css")}}'>
    <link id="color" rel="stylesheet" href='{{url("/panel/assets/css/light-1.css")}}' media="screen">
    <!-- Responsive css-->
    <link rel="stylesheet" type="text/css" href='{{url("/panel/assets/css/responsive.css")}}'>
  </head>
  <body main-theme-layout="rtl">
    <!-- Loader starts-->
    <div class="loader-wrapper">
      <div class="loader bg-white">
        <div class="whirly-loader"> </div>
      </div>
    </div>
    <!-- Loader ends-->
    <!-- page-wrapper Start-->
    <div class="page-wrapper">
      <div class="container-fluid">
        <!-- sign up page start-->
        <div class="auth-bg-video">
          <video id="bgvid" poster='{{url("../assets/images/other-images/coming-soon-bg.png")}}' playsinline="" autoplay="" muted="" loop="">
            <source src="http://admin.pixelstrap.com/endless/assets/video/auth-bg.mp4" type="video/mp4">
          </video>
          <div class="authentication-box">
{{--            <div class="text-center"><img src='{{url("/panel/assets/images/endless-logo.png")}}' alt=""></div>--}}
            <div class="card mt-4 p-4">
                @include('panel.messages')
                    <h4 class="text-center">کاربر جدید</h4>
                    <h6 class="text-center"> </h6>
                    <form class="theme-form" action="{{route('register')}}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label class="col-form-label">نام و نام خانوادگی</label>
                            <input class="form-control @error('name') is-invalid @enderror" name="name" type="text" value="{{old('name')}}">
                            @error('name')
                            <div class="alert alert-danger">{{$message}}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label class="col-form-label">شماره موبایل</label>
                            <input class="form-control @error('phone') is-invalid @enderror" name="phone" type="text" value="{{old('phone')}}">
                            @error('phone')
                            <div class="alert alert-danger">{{$message}}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label class="col-form-label">ایمیل</label>
                            <input class="form-control @error('email') is-invalid @enderror" name="email" type="email" value="{{old('email')}}">
                            @error('email')
                            <div class="alert alert-danger">{{$message}}</div>
                            @enderror
                        </div>
                      <div class="form-group">
                        <label class="col-form-label">رمز عبور</label>
                        <input class="form-control @error('password') is-invalid @enderror" type="password" name="password">
                          @error('password')
                          <div class="alert alert-danger">{{$message}}</div>
                          @enderror
                      </div>
                        <div class="form-group">
                            <label class="col-form-label">تکرار رمز عبور</label>
                            <input class="form-control @error('password_confirmation') is-invalid @enderror" type="password" name="password_confirmation">
                            @error('password_confirmation')
                            <div class="alert alert-danger">{{$message}}</div>
                            @enderror
                        </div>

                      <div class="form-row">
                        <div class="col-sm-4">
                          <button class="btn btn-primary" type="submit">ثبت نام</button>
                        </div>
                        <div class="col-sm-8">
                          <div class="text-left mt-2 m-l-20">آیا شما قبلا کاربر هستید؟  <a class="btn-link text-capitalize" href='{{route('login')}}'>ورود</a></div>
                  </div>
                </div>
                <div class="form-divider"></div>
                <div class="social mt-3">
                  <div class="form-group btn-showcase d-flex">
                    <button class="btn social-btn btn-fb d-inline-block"> <i class="fa fa-facebook"></i></button>
                    <button class="btn social-btn btn-twitter d-inline-block"><i class="fa fa-google"></i></button>
                    <button class="btn social-btn btn-google d-inline-block"><i class="fa fa-twitter"></i></button>
                    <button class="btn social-btn btn-github d-inline-block"><i class="fa fa-github"></i></button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
        <!-- sign up page ends-->
      </div>
    </div>
    <!-- page-wrapper Ends-->
    <!-- latest jquery-->
    <script src='{{url("/panel/assets/js/jquery-3.2.1.min.js")}}'></script>
    <!-- Bootstrap js-->
    <script src='{{url("/panel/assets/js/bootstrap/popper.min.js")}}'></script>
    <script src='{{url("/panel/assets/js/bootstrap/bootstrap.js")}}'></script>
    <!-- feather icon js-->
    <script src='{{url("/panel/assets/js/icons/feather-icon/feather.min.js")}}'></script>
    <script src='{{url("/panel/assets/js/icons/feather-icon/feather-icon.js")}}'></script>
    <!-- Sidebar jquery-->
    <script src='{{url("/panel/assets/js/sidebar-menu.js")}}'></script>
    <script src='{{url("/panel/assets/js/config.js")}}'></script>
    <!-- Plugins JS start-->
    <!-- Plugins JS Ends-->
    <!-- Theme js-->
    <script src='{{url("/panel/assets/js/script.js")}}'></script>
    <!-- Plugin used-->
  </body>

</html>
