<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::prefix('admin')->group(function () {
    Route::get('/', 'panel\PanelController@index')->name('panel.page');
    Route::get('/users', 'panel\UserController@index')->name('panel.users')->middleware('checkrole');
    Route::get('/profile/{user}', 'panel\UserController@edit')->name('admin.profile')->middleware('checkrole');
    Route::post('/profileUpdate/{user}', 'panel\UserController@update')->name('admin.profileUpdate');
    Route::get('/users/delete/{user}', 'panel\UserController@destroy')->name('admin.user.delete');
    Route::get('/users/status/{user}', 'panel\UserController@updatestatus')->name('admin.user.status');
});

Route::prefix('admin/categories')->group(function () {
    Route::get('/', 'panel\CategoryController@index')->name('admin.categories');
    Route::get('/creat', 'panel\CategoryController@create')->name('admin.categories.create');
    Route::post('/store', 'panel\CategoryController@store')->name('admin.categories.store');
    Route::get('/edit/{category}', 'panel\CategoryController@edit')->name('admin.categories.edit');
    Route::post('/update/{category}', 'panel\CategoryController@update')->name('admin.categories.update');
    Route::get('/destroy/{category}', 'panel\CategoryController@destroy')->name('admin.categories.destroy');
});
Route::prefix('admin/articles')->group(function () {
    Route::get('/', 'panel\ArticleController@index')->name('admin.articles');
    Route::get('/creat', 'panel\ArticleController@create')->name('admin.articles.create');
    Route::post('/store', 'panel\ArticleController@store')->name('admin.articles.store');
    Route::get('/edit/{article}', 'panel\ArticleController@edit')->name('admin.articles.edit');
    Route::post('/update/{article}', 'panel\ArticleController@update')->name('admin.articles.update');
    Route::get('/destroy/{article}', 'panel\ArticleController@destroy')->name('admin.articles.destroy');
    Route::get('/status/{article}', 'panel\ArticleController@updatestatus')->name('admin.articles.status');
});

Route::prefix('admin/comments')->group(function () {
    Route::get('/', 'panel\CommentController@index')->name('admin.comments');
    Route::get('/edit/{comment}', 'panel\CommentController@edit')->name('admin.comments.edit');
    Route::post('/update/{comment}', 'panel\CommentController@update')->name('admin.comments.update');
    Route::get('/destroy/{comment}', 'panel\CommentController@destroy')->name('admin.comments.destroy');
    Route::get('/status/{comment}', 'panel\CommentController@updatestatus')->name('admin.comments.status');
});

Route::get('/profile/{user}', 'UserController@edit')->name('profile') ;
Route::post('/update/{user}', 'UserController@update')->name('profileUpdate') ;

Route::group(['prefix' => 'laravel-filemanager', 'middleware' => ['web', 'auth']], function () {
    \UniSharp\LaravelFilemanager\Lfm::routes();
});

Route::get('/articles', 'front\ArticleController@index')->name('articles');
Route::get('/articles/{article}', 'front\ArticleController@show')->name('articles.detail');

Route::post('/comment/{article}', 'front\CommentController@store')->name('comment.store');


