'use strict';
(function() {
    var db = {
        loadData: function(filter) {
            return $.grep(this.clients, function(client) {
                return (!filter.Name || client.Name.indexOf(filter.Name) > -1)
                    && (!filter.Age || client.Age === filter.Age)
                    && (!filter.Address || client.Address.indexOf(filter.Address) > -1)
                    && (!filter.Country || client.Country === filter.Country)
                    && (filter.Married === undefined || client.Married === filter.Married);
            });
        },
        insertItem: function(insertingClient) {
            this.clients.push(insertingClient);
        },
        updateItem: function(updatingClient) { },

        deleteItem: function(deletingClient) {
            var clientIndex = $.inArray(deletingClient, this.clients);
            this.clients.splice(clientIndex, 1);
        }
    };
    window.db = db ;
    db.countries = [
        { Name: "ایران", Id: 1 },
        { Name: "امریکا", Id: 2 },
    ];
    db.clients = [
        {
            "نام": "Otto Clay",
            "سن": 61,
            "کشور": 6,
            "آدرس": "Ap #897-1459 Quam Avenue",
            "متاهل": false
        },
        {
            "نام": "Connor Johnston",
            "سن": 73,
            "کشور": 7,
            "آدرس": "Ap #370-4647 Dis Av.",
            "متاهل": false
        },
        {
            "نام": "Lacey Hess",
            "سن": 29,
            "کشور": 7,
            "آدرس": "Ap #365-8835 Integer St.",
            "متاهل": false
        },

    ];
    db.users = [
        {
            "ID": "x",
            "Account": "A758A693-0302-03D1-AE53-EEFE22855556",
            "نام": "Carson Kelley",
            "RegisterDate": "2002-04-20T22:55:52-07:00"
        },
        {
            "Account": "D89FF524-1233-0CE7-C9E1-56EFF017A321",
            "نام": "Prescott Griffin",
            "RegisterDate": "2011-02-22T05:59:55-08:00"
        },
     ];
}());
