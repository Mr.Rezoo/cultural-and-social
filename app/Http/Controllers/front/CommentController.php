<?php

namespace App\Http\Controllers\front;


use App\frontmodels\Article;
use App\frontmodels\Comment;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CommentController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Article $article)
    {

        $validateData = $request->validate([
            'name' => 'required',
            'email' => 'required',
            'body' => 'required',

        ]);
        $article->comments()->create([
            'name' => $request->name,
            'email' => $request->email,
            'body' => $request->body,

        ]);
        return back();
    }
}
